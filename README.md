# 3D SENSE in GPI
This is currently home to the 3D SENSE iterative reconstruction nodes for GPI.

## Installation
To develop against this repo, clone as `parallel` in your `~/gpi` directory.

    git clone https://gitlab.com/nckz/parallel-recon.git parallel
